#!/usr/bin/env bash

# todo: rename .xml .oat for nicer recognition

process() {
    stylesheet=$1
    outputname=$2

    for mode in odt ods odp; do
	xsltproc --stringparam mode $mode $stylesheet $stylesheet | xmlstarlet fo >| ${outputname}-${mode}.xml
    done
}

process createTextPropertiesTests.xsl          text-properties
process createParagraphPropertiesTests.xsl     paragraph-properties
process createTableCellPropertiesTests.xsl     table-cell-properties
process createSectionPropertiesTests.xsl       section-properties
process createListLevelPropertiesTests.xsl     list-level-properties
process createPageLayoutPropertiesTests.xsl    page-layout-properties
process createHeaderFooterPropertiesTests.xsl  header-footer-properties
process createTablePropertiesTests.xsl         table-properties
process createGraphicPropertiesTests.xsl       graphic-properties


