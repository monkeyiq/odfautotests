<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"
	xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
	xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
	xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
	xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
	xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
	xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"
	xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
	xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#"
	xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
	xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
	xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
	xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
	xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
	xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.example.org/documenttests" xmlns:t="http://www.example.org/documenttests">

	<xsl:import href="shared.xsl" />

	<xsl:param name="mode" select="'odt'" />
	<xsl:param name="test" select="'table'" />

	<xsl:param name="props" select="'paragraph'" />

	<t:testtemplates>
		<t:testtemplate name="background-color">
			<fo:background-color value="#339999" />
		</t:testtemplate>
		<t:testtemplate name="border">
			<fo:border value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-bottom">
			<fo:border-bottom value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-left">
			<fo:border-left value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-right">
			<fo:border-right value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-top">
			<fo:border-top value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="padding">
			<fo:padding value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-bottom">
			<fo:padding-bottom value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-left">
			<fo:padding-left value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-right">
			<fo:padding-right value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-top">
			<fo:padding-top value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="wrap-option">
			<fo:wrap-option value="no-wrap" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width">
			<fo:border value="5pt double #000000" />
			<s:border-line-width value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-bottom">
			<fo:border-bottom value="5pt double #000000" />
			<s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-left">
			<fo:border-left value="5pt double #000000" />
			<s:border-line-width-left value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-right">
			<fo:border-right value="5pt double #000000" />
			<s:border-line-width-right value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-top">
			<fo:border-top value="5pt double #000000" />
			<s:border-line-width-top value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="cell-protect">
			<s:cell-protect value="protected" />
		</t:testtemplate>
		<t:testtemplate name="decimal-places">
			<s:decimal-places value="5" />
		</t:testtemplate>
		<t:testtemplate name="diagonal-bl-tr">
			<s:diagonal-bl-tr value="3pt solid #800000" />
		</t:testtemplate>
		<t:testtemplate name="diagonal-bl-tr-widths">
			<s:diagonal-bl-tr-widths value="1pt 1pt 1pt" />
		</t:testtemplate>
		<t:testtemplate name="diagonal-tl-br">
			<s:diagonal-tl-br value="0.05pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="diagonal-tl-br-widths">
			<s:diagonal-tl-br-widths value="1pt 1pt 1pt" />
		</t:testtemplate>
		<t:testtemplate name="direction">
			<s:direction value="ttb" />
		</t:testtemplate>
		<t:testtemplate name="glyph-orientation-vertical">
			<s:glyph-orientation-vertical value="0" />
		</t:testtemplate>
		<t:testtemplate name="glyph-orientation-vertical-rad">
			<s:glyph-orientation-vertical value="0rad" />
		</t:testtemplate>
		<t:testtemplate name="print-content">
			<s:print-content value="false" />
		</t:testtemplate>
		<t:testtemplate name="repeat-content">
			<s:repeat-content value="true" />
		</t:testtemplate>
		<t:testtemplate name="rotation-align">
			<s:rotation-align value="bottom" />
			<s:rotation-angle value="270" />
		</t:testtemplate>
		<t:testtemplate name="rotation-angle">
			<s:rotation-align value="center" />
			<s:rotation-angle value="180" />
		</t:testtemplate>
		<t:testtemplate name="rotation-angle-rad">
			<s:rotation-align value="center" />
			<s:rotation-angle value="3.14rad" />
		</t:testtemplate>
		<t:testtemplate name="shadow">
			<s:shadow value="#808080 -0.0701in 0.0701in" />
		</t:testtemplate>
		<t:testtemplate name="shrink-to-fit">
			<s:shrink-to-fit value="true" />
		</t:testtemplate>
		<t:testtemplate name="text-align-source">
			<s:text-align-source value="fix" />
		</t:testtemplate>
		<t:testtemplate name="vertical-align">
			<s:vertical-align value="middle" />
		</t:testtemplate>
		<t:testtemplate name="writing-mode">
			<s:writing-mode value="lr-tb" />
		</t:testtemplate>
	</t:testtemplates>

	<xsl:output encoding="utf-8" indent="no" method="xml"
		omit-xml-declaration="no" />

	<xsl:template match="t:testtemplate">
		<xsl:variable name="family">
			<xsl:choose>
				<xsl:when test="$mode='ods'">
					<xsl:value-of select="'table-cell'" />
				</xsl:when>
				<xsl:when test="$mode='odp'">
					<xsl:value-of select="'graphic'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'table-cell'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<test name="{$mode}-{@name}">
			<input type="{$mode}1.2">
			  <o:styles>
			    <s:default-style s:family="table">
			      <s:table-properties table:border-model="collapsing"/>
<!--			      <s:table-cell-properties>
				<fo:decimal-places value="5" />
				</s:table-cell-properties>
-->				
			    </s:default-style>
			    
					<s:style s:name="standard" s:family="paragraph">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="graphic">
						<s:graphic-properties draw:stroke="none"
							draw:fill="none" />
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="table-cell">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="color" s:family="text" s:display-name="Text">
						<s:text-properties fo:color="#339999" />
					</s:style>
					<s:style s:name="table" s:family="table"
						s:master-page-name="Standard">
						<s:table-properties table:display="true"
							s:writing-mode="lr-tb" />
					</s:style>

					<s:style s:name="style" s:family="{$family}"
						s:display-name="TestStyle" s:parent-style-name="standard">
						<s:table-cell-properties>
							<xsl:for-each select="*">
								<xsl:attribute namespace="{namespace-uri()}" name="{name()}"><xsl:value-of
									select="@value" /></xsl:attribute>
							</xsl:for-each>
						</s:table-cell-properties>
					</s:style>
					
				</o:styles>
				<o:automatic-styles>
				  
					<s:style s:family="table" s:master-page-name="Standard"
						s:name="table">
						<s:table-properties s:writing-mode="lr-tb"
							table:display="true" />
					</s:style>
				</o:automatic-styles>
				<xsl:call-template name="body" />
			</input>
			<output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
				<file path="styles.xml">
					<xsl:for-each select="*">
						<xsl:variable name="selector"
							select="concat(&quot;//s:style[@s:display-name='TestStyle' or (not(@s:display-name) and @s:name='TestStyle')]/s:table-cell-properties/@&quot;,name())" />
						<xpath expr="boolean({$selector})" />
						<xsl:call-template name="xpaths">
							<xsl:with-param name="selector" select="$selector" />
							<xsl:with-param name="value" select="@value" />
							<xsl:with-param name="index" select="-1" />
						</xsl:call-template>
					</xsl:for-each>
				</file>
			</output>
			<pdf />
		</test>
	</xsl:template>

	<xsl:template match="/">
		<documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
			<xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*" />
			<xsl:apply-templates select="$tests" />
		</documenttests>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
