<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"
	xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
	xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
	xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
	xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
	xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
	xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"
	xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
	xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datastyle:1.0"
	xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0" xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#"
	xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
	xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
	xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
	xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
	xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
	xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.example.org/documenttests" xmlns:t="http://www.example.org/documenttests">

	<xsl:import href="shared.xsl" />

	<xsl:param name="mode" select="'odt'" />
	<xsl:param name="test" select="'paragraph'" />

	<t:testtemplates>
		<t:testtemplate name="color">
			<t:TestStyle>
			<fo:color value="#339999" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="background-color">
			<t:TestStyle>
			<fo:background-color value="#339999" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="background-color-transparent">
			<t:MiddleStyle>
			   <fo:background-color value="#FF0000" />
			</t:MiddleStyle>
			<t:TestStyle>
			   <fo:background-color value="transparent" />
                        </t:TestStyle>			   
		</t:testtemplate>
		<t:testtemplate name="country-language">
			<t:TestStyle>
			<fo:country value="NL" />
			<fo:language value="nl" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family">
			<t:TestStyle>
			<fo:font-family value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size">
			<t:TestStyle>
			<fo:font-size value="6pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-oblique">
			<t:TestStyle>
			<fo:font-style value="oblique" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-normal">
			<t:MiddleStyle>
			<fo:font-style value="italic" />
			</t:MiddleStyle>
			<t:TestStyle>
			<fo:font-style value="normal" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-variant-small-caps">
			<t:TestStyle>
			<fo:font-variant value="small-caps" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-variant-normal">
			<t:MiddleStyle>
			<fo:font-variant value="small-caps" />
			</t:MiddleStyle>
			<t:TestStyle>
			<fo:font-variant value="normal" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-weight-bold">
			<t:TestStyle>
			<fo:font-weight value="bold" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-weight-100">
			<t:TestStyle>
			<fo:font-weight value="100" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-weight-900">
			<t:TestStyle>
			<fo:font-weight value="900" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="hyphenate-true">
			<t:TestStyle>
			<fo:hyphenate value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="hyphenate-false">
			<t:TestStyle>
			<fo:hyphenate value="false" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="hyphenation-push-char-count-10">
			<t:TestStyle>
			<fo:hyphenation-push-char-count value="10" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="hyphenation-remain-char-count-10">
			<t:TestStyle>
			<fo:hyphenation-remain-char-count
				value="10" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="letter-spacing-normal">
			<t:MiddleStyle>
			<fo:letter-spacing value="1px" />
			</t:MiddleStyle>
			<t:TestStyle>
			<fo:letter-spacing value="normal" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="letter-spacing-1mm">
			<t:TestStyle>
			<fo:letter-spacing value="1mm" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="letter-spacing--1mm">
			<t:TestStyle>
			<fo:letter-spacing value="-1mm" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script">
			<t:TestStyle>
			<fo:script value="arab" />
			<fo:language value="ar" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-225">
			<t:TestStyle>
			<fo:script value="225" />
			<fo:language value="ar" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-es">
			<t:TestStyle>
			<fo:script value="es" />
			<fo:language value="es" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-shadow">
			<t:TestStyle>
			<fo:text-shadow value="3pt 4pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-transform-none">
			<t:MiddleStyle>
			<fo:text-transform value="uppercase" />
			</t:MiddleStyle>
			<t:TestStyle>
			<fo:text-transform value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-transform-lowercase">
			<t:TestStyle>
			<fo:text-transform value="lowercase" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-transform-uppercase">
			<t:TestStyle>
			<fo:text-transform value="uppercase" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-transform-capitalize">
			<t:TestStyle>
			<fo:text-transform value="capitalize" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="country-asian">
			<t:TestStyle>
			<s:country-asian value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="country-complex">
			<t:TestStyle>
			<s:country-complex value="SA" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-charset-x-symbol">
			<t:TestStyle>
			<s:font-charset value="x-symbol" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-charset-utf8">
			<t:TestStyle>
			<s:font-charset value="utf8" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-charset-utf-8">
			<t:TestStyle>
			<s:font-charset value="utf-8" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-charset-asian">
			<t:TestStyle>
			<s:font-charset-asian value="x-symbol" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-charset-complex">
			<t:TestStyle>
			<s:font-charset-complex value="x-symbol" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family-asian">
			<t:TestStyle>
			<s:font-family-asian value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family-complex">
			<t:TestStyle>
			<s:font-family-complex value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family-generic">
			<t:TestStyle>
			<s:font-family-generic value="modern" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family-generic-asian">
			<t:TestStyle>
			<s:font-family-generic-asian value="modern" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-family-generic-complex">
			<t:TestStyle>
			<s:font-family-generic-complex value="modern" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-name">
			<t:TestStyle>
			<s:font-name value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-name-asian">
			<t:TestStyle>
			<s:font-name-asian value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-name-complex">
			<t:TestStyle>
			<s:font-name-complex value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-pitch-fixed">
			<t:TestStyle>
			<s:font-pitch value="fixed" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-pitch-variable">
			<t:TestStyle>
			<s:font-pitch value="variable" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-pitch-asian">
			<t:TestStyle>
			<s:font-pitch-asian value="variable" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-pitch-complex">
			<t:TestStyle>
			<s:font-pitch-complex value="variable" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-relief-none">
			<t:MiddleStyle>
			<s:font-relief value="engraved" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:font-relief value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-relief-embossed">
			<t:TestStyle>
			<s:font-relief value="embossed" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-relief-engraved">
			<t:TestStyle>
			<s:font-relief value="engraved" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size-asian">
			<t:TestStyle>
			<s:font-size-asian value="3pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size-complex">
			<t:TestStyle>
			<s:font-size-complex value="3pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size-rel">
			<t:TestStyle>
			<s:font-size-rel value="16pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size-rel-asian">
			<t:TestStyle>
			<s:font-size-rel-asian value="16pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-size-rel-complex">
			<t:TestStyle>
			<s:font-size-rel-complex value="16pt" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-asian">
			<t:TestStyle>
			<s:font-style-asian value="italic" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-complex">
			<t:TestStyle>
			<s:font-style-complex value="italic" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-name">
			<t:TestStyle>
			<s:font-style-name value="Bold" />
			<fo:font-family value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-name-asian">
			<t:TestStyle>
			<s:font-style-name-asian value="Bold" />
			<fo:font-family value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-style-name-complex">
			<t:TestStyle>
			<s:font-style-name-complex value="Bold" />
			<fo:font-family value="Helvetica" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-weight-asian">
			<t:TestStyle>
			<s:font-weight-asian value="bold" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="font-weight-complex">
			<t:TestStyle>
			<s:font-weight-complex value="bold" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="language-asian">
			<t:TestStyle>
			<s:language-asian value="zh" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="language-complex">
			<t:MiddleStyle>
			<s:language-complex value="hi" />
			<s:country-complex value="IN" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:language-complex value="ar" />
			<s:country-complex value="SA" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="letter-kerning-false">
			<t:TestStyle>
			<s:letter-kerning value="false" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="letter-kerning-true">
			<t:MiddleStyle>
			<s:letter-kerning value="false" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:letter-kerning value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="rfc-language-tag">
			<t:TestStyle>
			<s:rfc-language-tag value="mn" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="rfc-language-tag-asian">
			<t:TestStyle>
			<s:rfc-language-tag-asian value="mn" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="rfc-language-tag-complex">
			<t:TestStyle>
			<s:rfc-language-tag-complex value="mn" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-asian">
			<t:TestStyle>
			<s:script-asian value="en" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-complex">
			<t:TestStyle>
			<s:script-complex value="en" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-type-latin">
			<t:TestStyle>
			<s:script-type value="latin" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="script-type-asian">
			<t:TestStyle>
			<s:script-type value="latin" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-blinking">
			<t:TestStyle>
			<s:text-blinking value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-combine-none">
			<t:MiddleStyle>
			<s:text-combine value="letters" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:text-combine value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-combine-letters">
			<t:TestStyle>
			<s:text-combine value="letters" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-combine-lines">
			<t:TestStyle>
			<s:text-combine value="lines" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-combine-end-char">
			<t:TestStyle>
			<s:text-combine-end-char value="Z" />
			<s:text-combine value="lines" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-combine-start-char">
			<t:TestStyle>
			<s:text-combine-start-char value="A" />
			<s:text-combine-end-char value="Z" />
			<s:text-combine value="lines" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-emphasize">
			<t:TestStyle>
			<s:text-emphasize value="disc below" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-color">
			<t:TestStyle>
			<s:text-line-through-color value="#339999" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-color-font">
			<t:TestStyle>
			<s:text-line-through-color value="font-color" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-mode-continuous">
			<t:TestStyle>
			<s:text-line-through-mode value="continuous" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-mode-skip-white-space">
			<t:TestStyle>
			<s:text-line-through-mode value="skip-white-space" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-style-solid">
			<t:TestStyle>
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-style-wave">
			<t:TestStyle>
			<s:text-line-through-style value="wave" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-text">
			<t:TestStyle>
			<s:text-line-through-style value="solid" />
			<s:text-line-through-text value="X" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-text-style">
			<t:TestStyle>
			<s:text-line-through-text-style value="color" />
			<s:text-line-through-text value="X" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-type-none">
			<t:MiddleStyle>
			<s:text-line-through-type value="double" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:text-line-through-type value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-type-single">
			<t:TestStyle>
			<s:text-line-through-type value="single" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-type-double">
			<t:TestStyle>
			<s:text-line-through-type value="double" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-width-auto">
			<t:TestStyle>
			<s:text-line-through-width value="auto" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-width-bold">
			<t:TestStyle>
			<s:text-line-through-width value="bold" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-line-through-width-thin">
			<t:TestStyle>
			<s:text-line-through-width value="thin" />
			<s:text-line-through-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-outline-true">
			<t:TestStyle>
			<s:text-outline value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-outline-false">
			<t:MiddleStyle>
			<s:text-outline value="true" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:text-outline value="false" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-overline-color">
			<t:TestStyle>
			<s:text-overline-color value="#339999" />
			<s:text-overline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-overline-mode">
			<t:TestStyle>
			<s:text-overline-mode value="continuous" />
			<s:text-overline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-overline-style">
			<t:TestStyle>
			<s:text-overline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-overline-type">
			<t:TestStyle>
			<s:text-overline-type value="single" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-overline-width">
			<t:TestStyle>
			<s:text-overline-width value="1pt" />
			<s:text-overline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-position-sub">
			<t:TestStyle>
			<s:text-position value="sub" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-position-sub-50">
			<t:TestStyle>
			<s:text-position value="sub 50%" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-position-super">
			<t:TestStyle>
			<s:text-position value="super" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-position-super-50">
			<t:TestStyle>
			<s:text-position value="super 50%" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-rotation-angle">
			<t:TestStyle>
			<s:text-rotation-angle value="90" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-rotation-scale-fixed">
			<t:TestStyle>
			<s:text-rotation-scale value="fixed" />
			<s:text-rotation-angle value="90" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-rotation-scale-line-height">
			<t:TestStyle>
			<s:text-rotation-scale value="line-height" />
			<s:text-rotation-angle value="90" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-scale">
			<t:TestStyle>
			<s:text-scale value="50%" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-scale-wide">
			<t:TestStyle>
			<s:text-scale value="200%" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-color-font-color">
			<t:MiddleStyle>
			<s:text-underline-color value="#FF0000" />
			<s:text-underline-style value="dotted" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:text-underline-color value="font-color" />
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-color">
			<t:TestStyle>
			<s:text-underline-color value="#339999" />
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-mode-continuous">
			<t:TestStyle>
			<s:text-underline-mode value="continuous" />
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-mode-skip-white-space">
			<t:TestStyle>
			<s:text-underline-mode value="skip-white-space" />
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-style-none">
			<t:TestStyle>
			<s:text-underline-style value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-style-dash">
			<t:TestStyle>
			<s:text-underline-style value="dash" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-style-solid">
			<t:TestStyle>
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-style-wave">
			<t:TestStyle>
			<s:text-underline-style value="wave" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-type-none">
			<t:MiddleStyle>
			<s:text-underline-type value="double" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:text-underline-type value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-type-single">
			<t:TestStyle>
			<s:text-underline-type value="single" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-type-double">
			<t:TestStyle>
			<s:text-underline-type value="double" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="text-underline-width">
			<t:TestStyle>
			<s:text-underline-width value="1pt" />
			<s:text-underline-style value="solid" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="use-window-font-color-true">
			<t:MiddleStyle>
			<s:use-window-font-color value="false" />
			</t:MiddleStyle>
			<t:TestStyle>
			<s:use-window-font-color value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="use-window-font-color-false">
			<t:TestStyle>
			<s:use-window-font-color value="false" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="display-true">
			<t:TestStyle>
			<text:display value="true" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="display-none">
			<t:TestStyle>
			<text:display value="none" />
			</t:TestStyle>
		</t:testtemplate>
		<t:testtemplate name="display-condition">
			<t:TestStyle>
			<text:display value="condition" />
			<text:condition value="none" />
			</t:TestStyle>
		</t:testtemplate>
	</t:testtemplates>

	<xsl:output encoding="utf-8" indent="no" method="xml"
		omit-xml-declaration="no" />


	<xsl:template match="t:testtemplate">
		<xsl:variable name="family">
			<xsl:choose>
				<xsl:when test="$mode='ods'">
					<xsl:value-of select="'table-cell'" />
				</xsl:when>
				<xsl:when test="$mode='odp'">
					<xsl:value-of select="'graphic'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'paragraph'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<test name="{$mode}-{@name}">
			<input type="{$mode}1.2">
				<o:styles>
					<s:style s:name="standard" s:family="paragraph">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="graphic">
						<s:graphic-properties draw:stroke="none"
							draw:fill="none" />
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="table-cell">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="color" s:family="text" s:display-name="Text">
						<s:text-properties fo:color="#339999" />
					</s:style>
					<s:style s:name="middle" s:family="{$family}"
						s:display-name="middle" s:parent-style-name="standard">
						<s:text-properties>
							<xsl:for-each select="t:MiddleStyle/*">
								<xsl:attribute namespace="{namespace-uri()}" name="{name()}"><xsl:value-of
									select="@value" /></xsl:attribute>
							</xsl:for-each>
						</s:text-properties>
					</s:style>
					<s:style s:name="style" s:family="{$family}"
						s:display-name="TestStyle" s:parent-style-name="middle">
						<s:text-properties>
							<xsl:for-each select="t:TestStyle/*">
								<xsl:attribute namespace="{namespace-uri()}" name="{name()}"><xsl:value-of
									select="@value" /></xsl:attribute>
							</xsl:for-each>
						</s:text-properties>
					</s:style>
					<s:style s:name="table" s:family="table"
						s:master-page-name="Standard">
						<s:table-properties table:display="true"
							s:writing-mode="lr-tb" />
					</s:style>
				</o:styles>
				<o:automatic-styles>
					<s:style s:family="table" s:master-page-name="Standard"
						s:name="table">
						<s:table-properties s:writing-mode="lr-tb"
							table:display="true" />
					</s:style>
				</o:automatic-styles>
				<xsl:call-template name="body" />
			</input>
			<output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
				<file path="styles.xml">
					<xsl:for-each select="t:TestStyle/*">
						<xsl:variable name="selector"
							select="concat(&quot;//s:style[@s:display-name='TestStyle' or (not(@s:display-name) and @s:name='TestStyle')]/s:text-properties/@&quot;,name())" />
						<xpath expr="boolean({$selector})" />
						<xsl:call-template name="xpaths">
							<xsl:with-param name="selector" select="$selector" />
							<xsl:with-param name="value" select="@value" />
							<xsl:with-param name="index" select="-1" />
						</xsl:call-template>
					</xsl:for-each>
				</file>
			</output>
			<pdf />
		</test>
	</xsl:template>

	<xsl:template match="/">
		<documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
			<xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*" />
			<xsl:apply-templates select="$tests" />
		</documenttests>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
