<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
	xmlns:anim="urn:oasis:names:tc:opendocument:xmlns:animation:1.0"
	xmlns:chart="urn:oasis:names:tc:opendocument:xmlns:chart:1.0"
	xmlns:config="urn:oasis:names:tc:opendocument:xmlns:config:1.0"
	xmlns:db="urn:oasis:names:tc:opendocument:xmlns:database:1.0" xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:dr3d="urn:oasis:names:tc:opendocument:xmlns:dr3d:1.0" xmlns:draw="urn:oasis:names:tc:opendocument:xmlns:drawing:1.0"
	xmlns:fo="urn:oasis:names:tc:opendocument:xmlns:xsl-fo-compatible:1.0"
	xmlns:form="urn:oasis:names:tc:opendocument:xmlns:form:1.0"
	xmlns:manifest="urn:oasis:names:tc:opendocument:xmlns:manifest:1.0"
	xmlns:meta="urn:oasis:names:tc:opendocument:xmlns:meta:1.0"
	xmlns:number="urn:oasis:names:tc:opendocument:xmlns:datas:1.0" xmlns:o="urn:oasis:names:tc:opendocument:xmlns:office:1.0"
	xmlns:odf="http://docs.oasis-open.org/ns/office/1.2/meta/odf#"
	xmlns:of="urn:oasis:names:tc:opendocument:xmlns:of:1.2"
	xmlns:presentation="urn:oasis:names:tc:opendocument:xmlns:presentation:1.0"
	xmlns:s="urn:oasis:names:tc:opendocument:xmlns:style:1.0" xmlns:script="urn:oasis:names:tc:opendocument:xmlns:script:1.0"
	xmlns:smil="urn:oasis:names:tc:opendocument:xmlns:smil-compatible:1.0"
	xmlns:svg="urn:oasis:names:tc:opendocument:xmlns:svg-compatible:1.0"
	xmlns:table="urn:oasis:names:tc:opendocument:xmlns:table:1.0"
	xmlns:text="urn:oasis:names:tc:opendocument:xmlns:text:1.0"
	xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns="http://www.example.org/documenttests" xmlns:t="http://www.example.org/documenttests">

	<xsl:import href="shared.xsl" />

	<xsl:param name="mode" select="'odt'" />
	<xsl:param name="test" select="'graphic'" />

	<xsl:param name="props" select="'paragraph'" />

	<t:testtemplatesSMALL>
		<t:testtemplate name="background-color">
			<fo:background-color value="#339999" />
		</t:testtemplate>
	</t:testtemplatesSMALL>
	
	<t:testtemplates>
		<t:testtemplate name="dr3d-ambient-color">
			<dr3d:ambient-color value="#339999" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-backface-culling-enabled">
			<dr3d:backface-culling value="enabled" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-backface-culling-disabled">
			<dr3d:backface-culling value="disabled" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-back-scale-50p">
			<dr3d:back-scale value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="dr3d-close-back-false">
			<dr3d:close-back value="false" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-close-back-true">
			<dr3d:close-back value="true" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-close-front-false">
			<dr3d:close-front value="false" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-close-front-true">
			<dr3d:close-front value="true" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-depth-in">
			<dr3d:depth value="1in" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-diffuse-color">
			<dr3d:diffuse-color value="#ff0000" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-edge-rounding-5p">
			<dr3d:edge-rounding value="5%" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-edge-rounding-5p">
			<dr3d:edge-rounding value="5%" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-edge-rounding-mode-correct">
			<dr3d:edge-rounding-mode value="correct" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-edge-rounding-mode-attractive">
			<dr3d:edge-rounding-mode value="attractive" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-emissive-color">
			<dr3d:emissive-color value="#ff0000" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-end-angle-deg">
			<dr3d:end-angle value="180deg" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-end-angle-rad">
			<dr3d:end-angle value="3.14rad" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-horizontal-segments-4">
			<dr3d:horizontal-segments value="4" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-horizontal-segments-128">
			<dr3d:horizontal-segments value="128" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-horizontal-segments-256">
			<dr3d:horizontal-segments value="256" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-lighting-mode-standard">
			<dr3d:lighting-mode value="standard" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-lighting-mode-double-sided">
			<dr3d:lighting-mode value="double-sided" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-normals-direction-normal">
			<dr3d:normals-direction value="normal" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-normals-direction-inverse">
			<dr3d:normals-direction value="inverse" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-normals-kind-object">
			<dr3d:normals-kind value="object" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-normals-kind-flat">
			<dr3d:normals-kind value="flat" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-normals-kind-sphere">
			<dr3d:normals-kind value="sphere" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-shadow-visible">
			<dr3d:shadow value="visible" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-shadow-hidden">
			<dr3d:shadow value="hidden" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-shininess-20p">
			<dr3d:shininess value="20%" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-shininess-80p">
			<dr3d:shininess value="80%" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-specular-color">
			<dr3d:specular-color value="#ff0000" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-filter-enabled">
			<dr3d:texture-filter value="enabled" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-filter-disabled">
			<dr3d:texture-filter value="disabled" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-x-object">
			<dr3d:texture-generation-mode-x value="object" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-x-parallel">
			<dr3d:texture-generation-mode-x value="parallel" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-x-sphere">
			<dr3d:texture-generation-mode-x value="sphere" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-y-object">
			<dr3d:texture-generation-mode-y value="object" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-y-parallel">
			<dr3d:texture-generation-mode-y value="parallel" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-generation-mode-y-sphere">
			<dr3d:texture-generation-mode-y value="sphere" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-kind-luminance">
			<dr3d:texture-kind value="luminance" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-kind-intensity">
			<dr3d:texture-kind value="intensity" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-kind-color">
			<dr3d:texture-kind value="color" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-mode-replace">
			<dr3d:texture-mode value="replace" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-mode-modulate">
			<dr3d:texture-mode value="modulate" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-texture-mode-blend">
			<dr3d:texture-mode value="blend" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-vertical-segments-4">
			<dr3d:vertical-segments value="4" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-vertical-segments-128">
			<dr3d:vertical-segments value="128" />
		</t:testtemplate>
		<t:testtemplate name="dr3d-vertical-segments-256">
			<dr3d:vertical-segments value="256" />
		</t:testtemplate>





		<t:testtemplate name="draw-auto-grow-height-true">
			<draw:auto-grow-height value="true" />
		</t:testtemplate>
		<t:testtemplate name="draw-auto-grow-height-false">
			<draw:auto-grow-height value="false" />
		</t:testtemplate>
		<t:testtemplate name="draw-auto-grow-width-true">
			<draw:auto-grow-width value="true" />
		</t:testtemplate>
		<t:testtemplate name="draw-auto-grow-width-false">
			<draw:auto-grow-width value="false" />
		</t:testtemplate>
		<t:testtemplate name="draw-blue-green-red">
			<draw:blue  value="5%" />
			<draw:green value="4%" />
			<draw:red   value="-2%" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-angle-deg">
		        <draw:caption-angle  value="10deg" />
		        <draw:caption-angle-type  value="fixed" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-angle-rad">
			<draw:caption-angle  value="1rad" />
		        <draw:caption-angle-type  value="fixed" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-angle-type-fixed">
		        <draw:caption-angle-type  value="fixed" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-angle-type-free">
		        <draw:caption-angle-type  value="free" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-in">
		        <draw:caption-escape  value="1in" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-cm">
		        <draw:caption-escape  value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-p">
		        <draw:caption-escape  value="10%" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-direction-horizontal">
		        <draw:caption-escape-direction  value="horizontal" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-direction-vertical">
		        <draw:caption-escape-direction  value="vertical" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-escape-direction-auto">
		        <draw:caption-escape-direction  value="auto" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-fit-line-length-false">
		        <draw:caption-fit-line-length  value="false" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-fit-line-length-true">
		        <draw:caption-fit-line-length  value="true" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-gap-in">
		        <draw:caption-gap  value="1in" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-gap-cm">
		        <draw:caption-gap  value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-line-length-cm">
      		        <draw:caption-line-length      value="30cm" />
      		        <draw:caption-fit-line-length  value="false" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-line-length-in">
      		        <draw:caption-line-length      value="10in" />
      		        <draw:caption-fit-line-length  value="false" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-type-straight-line">
      		        <draw:caption-type      value="straight-line" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-type-angled-line">
      		        <draw:caption-type      value="angled-line" />
		</t:testtemplate>
		<t:testtemplate name="draw-caption-type-angled-connector-line">
      		        <draw:caption-type      value="angled-connector-line" />
		</t:testtemplate>
		<t:testtemplate name="draw-color-inversion-true">
      		        <draw:color-inversion  value="true" />
		</t:testtemplate>
		<t:testtemplate name="draw-color-mode-greyscale">
      		        <draw:color-mode  value="greyscale" />
		</t:testtemplate>
		<t:testtemplate name="draw-color-mode-mono">
      		        <draw:color-mode  value="mono" />
		</t:testtemplate>
		<t:testtemplate name="draw-color-mode-watermark">
      		        <draw:color-mode  value="watermark" />
		</t:testtemplate>
		<t:testtemplate name="draw-color-mode-standard">
      		        <draw:color-mode  value="standard" />
		</t:testtemplate>
		<t:testtemplate name="draw-contrast-50p">
      		        <draw:contrast  value="50%" />
		</t:testtemplate>
		<t:testtemplate name="draw-decimal-places-5">
      		        <draw:decimal-places  value="5" />
		</t:testtemplate>
		<t:testtemplate name="draw-aspect-content">
      		        <draw:draw-aspect  value="content" />
		</t:testtemplate>
		<t:testtemplate name="draw-aspect-thumbnail">
      		        <draw:draw-aspect  value="thumbnail" />
		</t:testtemplate>
		<t:testtemplate name="draw-aspect-icon">
      		        <draw:draw-aspect  value="icon" />
		</t:testtemplate>
		<t:testtemplate name="draw-aspect-print-view">
      		        <draw:draw-aspect  value="print-view" />
		</t:testtemplate>
		<t:testtemplate name="draw-end-guide-in">
      		        <draw:end-guide  value="1in" />
		</t:testtemplate>
		<t:testtemplate name="draw-end-guide-cm">
      		        <draw:end-guide  value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="end-line-spacing-horizontal-in">
      		        <draw:end-line-spacing-horizontal  value="1in" />
		</t:testtemplate>
		<t:testtemplate name="end-line-spacing-horizontal-cm">
      		        <draw:end-line-spacing-horizontal  value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="end-line-spacing-vertical-in">
      		        <draw:end-line-spacing-vertical  value="1in" />
		</t:testtemplate>
		<t:testtemplate name="end-line-spacing-vertical-cm">
      		        <draw:end-line-spacing-vertical  value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-none">
      		        <draw:fill  value="none" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-solid">
      		        <draw:fill  value="solid" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-bitmap">
      		        <draw:fill  value="bitmap" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-gradient">
      		        <draw:fill  value="gradient" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-hatch">
      		        <draw:fill  value="hatch" />
		</t:testtemplate>
		<t:testtemplate name="draw-fill-color">
      		        <draw:fill-color  value="#ff0000" />
		</t:testtemplate>
		<t:testtemplate name="fraw-fill-gradient-name">
      		        <draw:fill-gradient-name  value="gradStyle" />
      		        <draw:fill  value="gradient" />
		</t:testtemplate>
		<t:testtemplate name="fraw-fill-hatch-name">
      		        <draw:fill-hatch-name  value="hatchStyle" />
      		        <draw:fill  value="hatch" />
		</t:testtemplate>
 		<t:testtemplate name="fraw-fill-hatch-solid-false">
      		        <draw:fill-hatch-solid  value="false" />
      		        <draw:fill-hatch-name  value="hatchStyle" />
      		        <draw:fill  value="hatch" />
		</t:testtemplate>
 		<t:testtemplate name="fraw-fill-hatch-solid-true">
      		        <draw:fill-hatch-solid  value="true" />
      		        <draw:fill-hatch-name  value="hatchStyle" />
      		        <draw:fill  value="hatch" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-height-in">
      		        <draw:fill-image-height  value="1in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-height-cm">
      		        <draw:fill-image-height  value="3cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-height-p">
      		        <draw:fill-image-height  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-name">
      		        <draw:fill-image-name  value="imageStyle" />
      		        <draw:fill  value="bitmap" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-top-left">
      		        <draw:fill-image-ref-point  value="top-left" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-top">
      		        <draw:fill-image-ref-point  value="top" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-top-right">
      		        <draw:fill-image-ref-point  value="top-right" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-left">
      		        <draw:fill-image-ref-point  value="left" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-center">
      		        <draw:fill-image-ref-point  value="center" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-right">
      		        <draw:fill-image-ref-point  value="right" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-bottom-left">
      		        <draw:fill-image-ref-point  value="bottom-left" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-bottom">
      		        <draw:fill-image-ref-point  value="bottom" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-ref-point-x-50p">
      		        <draw:fill-image-ref-point-x  value="50%" />
      		        <s:repeat  value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-width-in">
      		        <draw:fill-image-width  value="5in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-width-cm">
      		        <draw:fill-image-width  value="15cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fill-image-width-50p">
      		        <draw:fill-image-width  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fit-to-contour-true">
      		        <draw:fit-to-contour  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fit-to-contour-false">
      		        <draw:fit-to-contour  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fit-to-size-true">
      		        <draw:fit-to-size  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-fit-to-size-false">
      		        <draw:fit-to-size  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-display-border-true">
      		        <draw:frame-display-border  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-display-border-false">
      		        <draw:frame-display-border  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-display-scrollbar-true">
      		        <draw:frame-display-scrollbar  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-display-scrollbar-false">
      		        <draw:frame-display-scrollbar  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-margin-horizontal-10px">
      		        <draw:frame-margin-horizontal  value="10px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-frame-margin-vertical-10px">
      		        <draw:frame-margin-vertical  value="10px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-gamma-50p">
      		        <draw:gamma  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-gradient-step-count-4">
      		        <draw:gradient-step-count  value="4" />
		</t:testtemplate>
 		<t:testtemplate name="draw-gradient-step-count-128">
      		        <draw:gradient-step-count  value="128" />
		</t:testtemplate>
 		<t:testtemplate name="draw-gradient-step-count-512">
      		        <draw:gradient-step-count  value="512" />
		</t:testtemplate>
 		<t:testtemplate name="draw-guide-distance">
      		        <draw:guide-distance  value="10px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-guide-overhang-in">
      		        <draw:guide-overhang  value="1in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-guide-overhang-cm">
      		        <draw:guide-overhang  value="3cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-image-opacity">
      		        <draw:image-opacity  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-line-distance">
      		        <draw:line-distance  value="10px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-luminance-50p">
      		        <draw:luminance  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-end">
      		        <draw:marker-end  value="endStyle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-end-center-true">
      		        <draw:marker-end-center  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-end-center-false">
      		        <draw:marker-end-center  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-end-width-in">
      		        <draw:marker-end-width  value="1in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-end-width-cm">
      		        <draw:marker-end-width  value="3cm" />
		</t:testtemplate>

 		<t:testtemplate name="draw-marker-start">
      		        <draw:marker-start  value="startStyle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-start-center-true">
      		        <draw:marker-start-center  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-start-center-false">
      		        <draw:marker-start-center  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-start-width-in">
      		        <draw:marker-start-width  value="1in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-marker-start-width-cm">
      		        <draw:marker-start-width  value="3cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-align-automatic">
      		        <draw:measure-align  value="automatic" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-align-left-outside">
      		        <draw:measure-align  value="left-outside" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-align-inside">
      		        <draw:measure-align  value="inside" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-align-right-outside">
      		        <draw:measure-align  value="right-outside" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-vertical-align-automatic">
      		        <draw:measure-vertical-align  value="automatic" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-vertical-align-above">
      		        <draw:measure-vertical-align  value="above" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-vertical-align-below">
      		        <draw:measure-vertical-align  value="below" />
		</t:testtemplate>
 		<t:testtemplate name="draw-measure-vertical-align-center">
      		        <draw:measure-vertical-align  value="center" />
		</t:testtemplate>
 		<t:testtemplate name="draw-opacity-50p">
      		        <draw:opacity  value="50%" />
		</t:testtemplate>
 		<t:testtemplate name="draw-opacity-name">
      		        <draw:opacity-name  value="opacityStyle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-parallel-false">
      		        <draw:parallel  value="false" />
		</t:testtemplate>
 		<t:testtemplate name="draw-parallel-true">
      		        <draw:parallel  value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-placing-above">
      		        <draw:placing  value="above" />
		</t:testtemplate>
 		<t:testtemplate name="draw-placing-below">
      		        <draw:placing  value="below" />
		</t:testtemplate>
 		<t:testtemplate name="draw-secondary-fill-color">
      		        <draw:secondary-fill-color  value="#ff0000" />
		</t:testtemplate>
 		<t:testtemplate name="draw-shadow-visible">
      		        <draw:shadow  value="visible" />
		</t:testtemplate>
 		<t:testtemplate name="draw-shadow-color">
      		        <draw:shadow        value="visible" />
      		        <draw:shadow-color  value="#ff0000" />
		</t:testtemplate>
 		<t:testtemplate name="draw-shadow-offset-x">
      		        <draw:shadow           value="visible" />
      		        <draw:shadow-color     value="#ff0000" />
      		        <draw:shadow-offset-x  value="3px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-shadow-offset-y">
      		        <draw:shadow           value="visible" />
      		        <draw:shadow-color     value="#ff0000" />
      		        <draw:shadow-offset-y  value="3px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-show-unit">
      		        <draw:show-unit        value="true" />
		</t:testtemplate>
 		<t:testtemplate name="draw-start-guide-in">
      		        <draw:start-guide        value="1in" />
		</t:testtemplate>
 		<t:testtemplate name="draw-start-guide-cm">
      		        <draw:start-guide        value="3cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-start-line-spacing-horizontal">
      		        <draw:start-line-spacing-horizontal        value="10px" />
		</t:testtemplate>
 		<t:testtemplate name="draw-start-line-spacing-vertical">
      		        <draw:start-line-spacing-vertical        value="10px" />
		</t:testtemplate>

 		<t:testtemplate name="draw-stroke-dash">
      		        <draw:stroke        value="dash" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-solid">
      		        <draw:stroke        value="solid" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-dashstyle">
      		        <draw:stroke-dash   value="dashStyle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-dash-names">
      		        <draw:stroke-dash-names   value="dashStyle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-linejoin-miter">
      		        <draw:stroke-linejoin   value="miter" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-linejoin-round">
      		        <draw:stroke-linejoin   value="round" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-linejoin-bevel">
      		        <draw:stroke-linejoin   value="bevel" />
		</t:testtemplate>
 		<t:testtemplate name="draw-stroke-linejoin-middle">
      		        <draw:stroke-linejoin   value="middle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-symbol-color">
      		        <draw:symbol-color   value="#ff0000" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-horizontal-align-left">
      		        <draw:textarea-horizontal-align   value="left" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-horizontal-align-center">
      		        <draw:textarea-horizontal-align   value="center" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-horizontal-align-right">
      		        <draw:textarea-horizontal-align   value="right" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-horizontal-align-justify">
      		        <draw:textarea-horizontal-align   value="justify" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-vertical-align-top">
      		        <draw:textarea-vertical-align   value="top" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-vertical-align-middle">
      		        <draw:textarea-vertical-align   value="middle" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-vertical-align-bottom">
      		        <draw:textarea-vertical-align   value="bottom" />
		</t:testtemplate>
 		<t:testtemplate name="draw-textarea-vertical-align-justify">
      		        <draw:textarea-vertical-align   value="justify" />
		</t:testtemplate>
 		<t:testtemplate name="draw-tile-repeat-offset">
      		        <draw:tile-repeat-offset   value="50% horizontal" />
      		        <s:repeat   value="repeat" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-mm">
      		        <draw:unit   value="mm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-cm">
      		        <draw:unit   value="cm" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-m">
      		        <draw:unit   value="m" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-km">
      		        <draw:unit   value="km" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-pt">
      		        <draw:unit   value="pt" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-pc">
      		        <draw:unit   value="pc" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-in">
      		        <draw:unit   value="inch" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-ft">
      		        <draw:unit   value="ft" />
		</t:testtemplate>
 		<t:testtemplate name="draw-unit-mi">
      		        <draw:unit   value="mi" />
		</t:testtemplate>

		<!--
skip draw:visible-area-height 20.169, 
skip draw:visible-area-left 20.170, 
skip draw:visible-area-top 20.171, 
skip draw:visible-area-width 20.172, 
-->		    
 		<t:testtemplate name="draw-wrap-influence-on-position-iterative">
      		        <draw:wrap-influence-on-position value="iterative" />
		</t:testtemplate>
 		<t:testtemplate name="draw-wrap-influence-on-position-once-concurrent">
      		        <draw:wrap-influence-on-position value="once-concurrent" />
		</t:testtemplate>
 		<t:testtemplate name="draw-wrap-influence-on-position-once-successive">
      		        <draw:wrap-influence-on-position value="once-successive" />
		</t:testtemplate>

 <!-- ////////////////// -->
 <!-- ////////////////// -->
 <!-- ////////////////// -->
		     
		<t:testtemplate name="background-color">
			<fo:background-color value="#339999" />
		</t:testtemplate>
		<t:testtemplate name="border">
			<fo:border value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-bottom">
			<fo:border-bottom value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-left">
			<fo:border-left value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-right">
			<fo:border-right value="0.06pt solid #000000" />
		</t:testtemplate>
		<t:testtemplate name="border-top">
			<fo:border-top value="0.06pt solid #000000" />
		</t:testtemplate>

<!-- fo:clip 20.179, -->

		<t:testtemplate name="margin">
			<fo:margin value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="margin-bottom">
			<fo:margin-bottom value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="margin-left">
			<fo:margin-left value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="margin-right">
			<fo:margin-right value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="margin-top">
			<fo:margin-top value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-height-in">
			<fo:max-height value="1in" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-height-cm">
			<fo:max-height value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-height-50p">
			<fo:max-height value="50%" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-width-in">
			<fo:max-width value="1in" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-width-cm">
			<fo:max-width value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="fo-max-width-50p">
			<fo:max-width value="50%" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-height-in">
			<fo:min-height value="1in" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-height-cm">
			<fo:min-height value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-height-50p">
			<fo:min-height value="50%" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-width-in">
			<fo:min-width value="1in" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-width-cm">
			<fo:min-width value="3cm" />
		</t:testtemplate>
		<t:testtemplate name="fo-min-width-50p">
			<fo:min-width value="50%" />
		</t:testtemplate>
		<t:testtemplate name="padding">
			<fo:padding value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-bottom">
			<fo:padding-bottom value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-left">
			<fo:padding-left value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-right">
			<fo:padding-right value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="padding-top">
			<fo:padding-top value="0.0299in" />
		</t:testtemplate>
		<t:testtemplate name="wrap-option">
			<fo:wrap-option value="no-wrap" />
		</t:testtemplate>
		<t:testtemplate name="background-transparency">
			<s:background-transparency value="50%" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width">
			<fo:border value="5pt double #000000" />
			<s:border-line-width value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-bottom">
			<fo:border-bottom value="5pt double #000000" />
			<s:border-line-width-bottom value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-left">
			<fo:border-left value="5pt double #000000" />
			<s:border-line-width-left value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-right">
			<fo:border-right value="5pt double #000000" />
			<s:border-line-width-right value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="border-line-width-top">
			<fo:border-top value="5pt double #000000" />
			<s:border-line-width-top value="0.4mm 0.0299in 0.7mm" />
		</t:testtemplate>
		<t:testtemplate name="editable">
			<s:editable value="false" />
		</t:testtemplate>
		<t:testtemplate name="protect">
			<s:editable value="true" />
		</t:testtemplate>

 <!--  style:flow-with-text 20.259,  -->
 <!--  style:horizontal-pos 20.290,  -->
 <!-- style:horizontal-rel 20.291,  -->

		<t:testtemplate name="style-mirror-nh">
			<s:mirror value="horizontal" />
		</t:testtemplate>
		<t:testtemplate name="style-mirror-vhon">
			<s:mirror value="vertical horizontal-on-even" />
		</t:testtemplate>
		<t:testtemplate name="style-number-wrapped-paragraphs">
		  <s:number-wrapped-paragraphs value="2" />
		  <s:wrap value="right" />
		</t:testtemplate>

 <!-- style:number-wrapped-paragraphs 20.318,  -->

		<t:testtemplate name="style-overflow-behavior">
			<s:overflow-behavior value="clip" />
		</t:testtemplate>
		<t:testtemplate name="style-overflow-behavior">
			<s:overflow-behavior value="auto-create-new-frame" />
		</t:testtemplate>
		<t:testtemplate name="style-print-content-false">
			<s:print-content value="false" />
		</t:testtemplate>
		<t:testtemplate name="style-print-content-true">
			<s:print-content value="true" />
		</t:testtemplate>
		<t:testtemplate name="style-protect-content">
			<s:protect value="content" />
		</t:testtemplate>
		<t:testtemplate name="style-protect-position">
			<s:protect value="position" />
		</t:testtemplate>
		<t:testtemplate name="style-protect-size">
			<s:protect value="size" />
		</t:testtemplate>
		<t:testtemplate name="style-rel-height-50p">
			<s:rel-height value="50%" />
		</t:testtemplate>
		<t:testtemplate name="style-rel-width-50p">
			<s:rel-width value="50%" />
		</t:testtemplate>
		<t:testtemplate name="style-repeat-no">
			<s:repeat value="no-repeat" />
		</t:testtemplate>
		<t:testtemplate name="style-repeat-repeat">
			<s:repeat value="repeat" />
		</t:testtemplate>
		<t:testtemplate name="style-repeat-stretch">
			<s:repeat value="stretch" />
		</t:testtemplate>
		<t:testtemplate name="style-run-through-foreground">
			<s:run-through value="foreground" />
			<s:wrap        value="run-through" />
		</t:testtemplate>
		<t:testtemplate name="style-run-through-background">
			<s:run-through value="background" />
			<s:wrap        value="run-through" />
		</t:testtemplate>
		<t:testtemplate name="shadow">
			<s:shadow value="#808080 -0.0701in 0.0701in" />
		</t:testtemplate>
		<t:testtemplate name="style-shrink-to-fit-false">
			<s:shrink-to-fit value="false" />
		</t:testtemplate>
		<t:testtemplate name="style-shrink-to-fit-true">
			<s:shrink-to-fit value="true" />
		</t:testtemplate>

<!-- style:vertical-pos 20.387,  -->
<!-- style:vertical-rel 20.388,  -->

		<t:testtemplate name="style-wrap-none">
			<s:wrap value="none" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-left">
			<s:wrap value="left" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-right">
			<s:wrap value="right" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-parallel">
			<s:wrap value="parallel" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-dynamic">
			<s:wrap value="dynamic" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-run-through">
			<s:wrap value="run-through" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-biggest">
			<s:wrap value="biggest" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-contour-false">
			<s:wrap-contour value="false" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-contour-true">
			<s:wrap-contour value="true" />
		</t:testtemplate>

		<t:testtemplate name="style-wrap-contour-mode-full">
			<s:wrap-contour-mode value="full" />
			<s:wrap value="left" />
			<s:wrap-contour value="true" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-contour-mode-outside">
			<s:wrap-contour-mode value="outside" />
			<s:wrap value="left" />
			<s:wrap-contour value="true" />
		</t:testtemplate>
		
		<t:testtemplate name="style-wrap-dynamic-threshold-in">
			<s:wrap-dynamic-threshold value="1in" />
			<s:wrap value="dynamic" />
		</t:testtemplate>
		<t:testtemplate name="style-wrap-dynamic-threshold-cm">
			<s:wrap-dynamic-threshold value="3cm" />
			<s:wrap value="dynamic" />
		</t:testtemplate>
		<t:testtemplate name="writing-mode">
			<s:writing-mode value="lr-tb" />
		</t:testtemplate>


<!-- 		

svg:fill-rule 20.396, 
svg:height 20.397.1, 
svg:stroke-color 20.398, 
svg:stroke-linecap 20.164, 
svg:stroke-opacity 20.399, 
svg:stroke-width 20.400, 
svg:width 20.403, 
svg:x 20.401, 
svg:y 20.402.1, 
text:anchor-page-number 20.407, 
text:anchor-type 20.408, 
text:animation 20.409, 
text:animation-delay 20.410, 
text:animation-direction 20.411, 
text:animation-repeat 20.412, 
text:animation-start-inside 20.413, 
text:animation-steps 20.414 and 
text:animation-stop-inside 20.415.
-->


	</t:testtemplates>

	<xsl:output encoding="utf-8" indent="no" method="xml"
		omit-xml-declaration="no" />

	<xsl:template match="t:testtemplate">
		<xsl:variable name="family">
			<xsl:choose>
				<xsl:when test="$mode='ods'">
					<xsl:value-of select="'table-cell'" />
				</xsl:when>
				<xsl:when test="$mode='odp'">
					<xsl:value-of select="'graphic'" />
				</xsl:when>
				<xsl:otherwise>
					<xsl:value-of select="'paragraph'" />
				</xsl:otherwise>
			</xsl:choose>
		</xsl:variable>
		<test name="{$mode}-{@name}">
			<input type="{$mode}1.2">
			  <o:styles>
			               <s:style s:name="teststyle" s:family="graphic" s:parent-style-name="Graphics">
					 <s:graphic-properties >
					   <xsl:for-each select="*">
					     <xsl:attribute namespace="{namespace-uri()}" name="{name()}"><xsl:value-of
					     select="@value" /></xsl:attribute>
					   </xsl:for-each>
					 </s:graphic-properties>
				       </s:style>
				       
					<s:style s:name="standard" s:family="paragraph">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="graphic">
						<s:graphic-properties draw:stroke="none"
							draw:fill="none" />
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="standard" s:family="table-cell">
						<s:text-properties fo:font-size="12pt"
							s:font-name="Helvetica" />
					</s:style>
					<s:style s:name="color" s:family="text" s:display-name="Text">
						<s:text-properties fo:color="#339999" />
					</s:style>
					<s:style s:name="style" s:family="{$family}"
						s:display-name="paraStyle" s:parent-style-name="standard">
						<s:paragraph-properties/>
					</s:style>
					<s:style s:name="table" s:family="table"
						s:master-page-name="Standard">
						<s:table-properties table:display="true"
							s:writing-mode="lr-tb" />
					</s:style>
				</o:styles>
				<o:automatic-styles>
					<s:style s:family="table" s:master-page-name="Standard"
						s:name="table">
						<s:table-properties s:writing-mode="lr-tb"
							table:display="true" />
					</s:style>
				</o:automatic-styles>
				<xsl:call-template name="body" />
			</input>
			<output types="{$mode}1.0 {$mode}1.1 {$mode}1.2 {$mode}1.2ext">
				<file path="styles.xml">
					<xsl:for-each select="*">
						<xsl:variable name="selector"
							select="concat(&quot;//s:style[@s:display-name='teststyle' or (not(@s:display-name) and @s:name='teststyle')]/s:graphic-properties/@&quot;,name())" />
						<xpath expr="boolean({$selector})" />
						<xsl:call-template name="xpaths">
							<xsl:with-param name="selector" select="$selector" />
							<xsl:with-param name="value" select="@value" />
							<xsl:with-param name="index" select="-1" />
						</xsl:call-template>
					</xsl:for-each>
				</file>
			</output>
			<pdf />
		</test>
	</xsl:template>

	<xsl:template match="/">
		<documenttests xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
			xsi:schemaLocation="http://www.example.org/documenttests ../documenttests.xsd">
			<xsl:variable name="tests" select="/xsl:stylesheet/t:testtemplates/*" />
			<xsl:apply-templates select="$tests" />
		</documenttests>
	</xsl:template>

	<xsl:template match="@*|node()">
		<xsl:copy>
			<xsl:apply-templates select="@*|node()" />
		</xsl:copy>
	</xsl:template>
</xsl:stylesheet>
